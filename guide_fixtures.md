*Si nécessaire :* 
```shell
composer require orm-fixtures --dev
```
### Créer une fixture
```shell
php bin/console make:fixtures
```
Pour modifier la table Job on nommera le fichier **ClassFixture.php***

### Injecter les données dans la BDD
```shell
php bin/console doctrine:fixtures:load
```
Cette commande créé un dossier **DataFixtures** dans ***src*** avec à l'intérieur :
- AppFixtures.php *(on peut le supprimer)*
- JobFixtures.php

**Pour utiliser la librairie *[Faker](https://github.com/fzaninotto/Faker) :***

```shell
composer require fzaninotto/faker
```

Si je veux générer des emplois aléatoires, dans **JobFixtures.php** je code :

```php
<?php

namespace App\DataFixtures;

use App\Entity\Job;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class JobFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // choix de la langue
        $faker = Factory::create('fr_FR');

        // génération de 10 emplois aléatoires
        for ($i = 0; $i <= 10; $i++) {
            $job = new Job();
            $job->setName($faker->jobTitle);
            $manager->persist($job);
        }
        $manager->flush();
    }
}
```