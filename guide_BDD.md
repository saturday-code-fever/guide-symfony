# Créer une base de données avec Symfony
made by #Liswag

### I - Créer une base de données  vide
Dans ***.env.local*** on modifie la connexion à la base de données :
```DATABASE_URL=mysql://*db_user*:*db_password*@127.0.0.1:*port*/*db_name*?serverVersion=*version*```

Pour connaître sa version :
```shell  
sudo mysql -u root  
```  
*Avec la version 10.4.14 de MariaDB ça donne : **mariadb-10.4.14***  

**Pour créer la base de données :**
```shell
php bin/console doctrine:database:create
```  
*Et pour la supprimer :*  
 ```shell
 php bin/console doctrine:database:drop --force
```

### II - Ajouter des tables

#### A - Créer des tables

Une fois la base de données créée on doit créer ses tables avec des Entités
```shell  
php bin/console make:entity  
```  
```shell  
php bin/console m:e  
```  
Cette commande demande d'abord le nom qu'on veut donner à l'entité/table. Elle demande ensuite un nom de propriété/colonne à ajouter, son type, sa longueur et si le champ peut être *null*.

***Attention :*** *le nom d'une entité prend une majuscule mais pas ses propriétés !*

**S'il y a une relation entre deux tables :**
On ajoute la propriété/colonne concernée comme décrit ***mais*** on lui donne le type ***relation***. Ensuite on indique l'autre entité/table impliquée dans la relation et le type de relation.

Quand on met fin à la commande, pour une entité qu'on aurait appelée "User" par exemple elle va créer :
- un fichier **User.php** dans ***src/Entity*** 
qui contiendra la classe User avec typage, getters et setters déjà prêts !
- un fichier **UserRepository.php** dans ***src/Repository***

#### B - Les ajouter dans la base de données
```shell  
php bin/console make:migration
```
```shell  
php bin/console m:mig
```
puis pour rendre la migration effective dans la BDD :
```shell  
php bin/console doctrine:migration:migrate
```
```shell  
php bin/console d:m:m
```

### IV - Créer les Contrôleurs correspondant

Dans une architecture Modèle-Vue-Contrôleur, la BDD et ses tables correspondent au **Modèle**. Pour lier le Modèle et la Vue, on doit créer un **Contrôleur** pour chacune des tables de la BDD. 

```shell  
php bin/console make:controller  
```  
```shell  
php bin/console m:con  
```  
Cette commande demande le nom à donner au Contrôleur (en CamelCase). 
Pour un Contrôleur nommé "User" elle crééra :
- un fichier **UserController.php** dans le dossier  ***src/Controller***
- un dossier **user** dans le dossier ***templates***, contenant un fichier **index.html.twig**

La route vers _**user/index.html.twig**_ est déjà créée dans **UserController.php**

**Ce Contrôleur permettra entre autres de gérer les opérations CRUD.**